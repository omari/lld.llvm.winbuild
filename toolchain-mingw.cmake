# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Windows)


# Choose an appropriate compiler prefix

set(COMPILER_PREFIX "x86_64-w64-mingw32")

# which compilers to use for C and C++
SET(CMAKE_C_COMPILER ${COMPILER_PREFIX}-gcc)
SET(CMAKE_CXX_COMPILER ${COMPILER_PREFIX}-g++)
SET(CMAKE_RC_COMPILER ${COMPILER_PREFIX}-windres)
SET(CMAKE_AR:FILEPATH ${COMPILER_PREFIX}-ar)
SET(CMAKE_RANLIB:FILEPATH ${COMPILER_PREFIX}-ranlib)

# target environment location
SET(CMAKE_FIND_ROOT_PATH /usr/bin/${COMPILER_PREFIX} /usr/bin /builds/omari/lld.llvm.winbuild/inst)

# adjust the default behaviour of the FIND_XXX() commands:
# search headers and libraries in the target environment, search 
# programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)